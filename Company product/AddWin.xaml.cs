﻿using Company_product.Forms;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Company_product
{
    

    public partial class AddWin : Window
    {
        NpgsqlConnection connection;
        private bool check;

        public AddWin(int tab, NpgsqlConnection connect, bool flag)
        {
            InitializeComponent();
            connection = connect;
            check = flag;
            enb_btn();
        }


        private void enb_btn()
        {
            if (check == true) //изменения
            {
                id_firm_box.IsEnabled = true;
                lab_firm.IsEnabled = true;

                id_customer_box.IsEnabled = true;
                lab_customer.IsEnabled = true;

                id_team_box.IsEnabled = true;
                lab_team.IsEnabled = true;

                id_associate_box.IsEnabled = true;
                lab_asociate.IsEnabled = true;

                id_project_box.IsEnabled = true;
                lab_project.IsEnabled = true;

            }
            else  //добовление
            {
                id_firm_box.IsEnabled = false;
                lab_firm.IsEnabled = false;

                id_customer_box.IsEnabled = false;
                lab_customer.IsEnabled = false;

                id_team_box.IsEnabled = false;
                lab_team.IsEnabled = false;

                id_associate_box.IsEnabled = false;
                lab_asociate.IsEnabled = false;

                id_project_box.IsEnabled = false;
                lab_project.IsEnabled = false;
            }
        }

        

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
         
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (check == false)//если false - то добавление, а если true то изменения
            {
                string commandString = "select C_firm(@name_firm, @date_create);";
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("name_firm", name_value.Text);
                    command.Parameters.AddWithValue("date_create", Convert.ToDateTime(date_create_value.Text));
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                name_value.Clear();
            }
            if (check == true)  //если false - то добавление, а если true то изменения
            {
                string commandString = "select e_firm(@id_firm, @name_firm, @date_create);";
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_firm",Convert.ToInt32(id_firm_box.Text));
                    command.Parameters.AddWithValue("name_firm", name_value.Text);
                    command.Parameters.AddWithValue("date_create", Convert.ToDateTime(date_create_value.Text));
                    command.ExecuteNonQuery();
                    MessageBox.Show("Изменение данных произведено");
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                name_value.Clear();
            }

        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
           
            if (check == false)//добавление
            {
                string commandString = "select C_customer(@name_company, @city_customer, @profit_of_year)";
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("name_company", nameCast_value.Text);
                    command.Parameters.AddWithValue("city_customer", city_value.Text);
                    command.Parameters.AddWithValue("profit_of_year", Convert.ToInt32(profit_value.Text));
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                nameCast_value.Clear();
                city_value.Clear();
                profit_value.Clear();
            }
            else //изменения
            {
                string commandString = "select E_customer(@id_customer, @name_company, @city, @profit_of_year)";
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_customer", Convert.ToInt32(id_customer_box.Text));
                    command.Parameters.AddWithValue("name_company", nameCast_value.Text);
                    command.Parameters.AddWithValue("city", city_value.Text);
                    command.Parameters.AddWithValue("profit_of_year", Convert.ToInt32(profit_value.Text));
                    command.ExecuteNonQuery();
                    MessageBox.Show("Изменение данных произведено");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                nameCast_value.Clear();
                city_value.Clear();
                profit_value.Clear();
            }

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (check == false)//добавление
            {
                string commandString = "select C_team(@id_firm, @quantity_human, @name_team)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_firm", Convert.ToInt32(idFirm_value.Text));
                    command.Parameters.AddWithValue("quantity_human", Convert.ToInt32(quantity_value.Text));
                    command.Parameters.AddWithValue("name_team", nameTeam_value.Text);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                idFirm_value.Clear();
                quantity_value.Clear();
                nameTeam_value.Clear();
            }
            else //изменения
            {
                string commandString = "select E_team(@id_team ,@id_firm, @quantity_human, @name_team)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_team", Convert.ToInt32(id_team_box.Text));
                    command.Parameters.AddWithValue("id_firm", Convert.ToInt32(idFirm_value.Text));
                    command.Parameters.AddWithValue("quantity_human", Convert.ToInt32(quantity_value.Text));
                    command.Parameters.AddWithValue("name_team", nameTeam_value.Text);
                    command.ExecuteNonQuery();
                    MessageBox.Show("Изменение данных произведено");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                idFirm_value.Clear();
                quantity_value.Clear();
                nameTeam_value.Clear();
            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (check == false)//добавление
            {
                string commandString = "select C_associate(@surname, @name, @id_team, @particip_in_projects, @experience)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("surname", surname_value.Text);
                    command.Parameters.AddWithValue("name", nameA_value.Text);
                    command.Parameters.AddWithValue("id_team", Convert.ToInt32(idTeam_value.Text));
                    command.Parameters.AddWithValue("particip_in_projects", Convert.ToInt32(partProj_value.Text));
                    command.Parameters.AddWithValue("experience", Convert.ToInt32(exp_value.Text));
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                surname_value.Clear();
                nameA_value.Clear();
                idTeam_value.Clear();
                partProj_value.Clear();
                exp_value.Clear();
            }
            else //изменения
            {
                string commandString = "select e_associate(@id_associate, @surname, @name, @id_team, @particip_in_projects, @experience)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_associate", Convert.ToInt32(id_associate_box.Text));
                    command.Parameters.AddWithValue("surname", surname_value.Text);
                    command.Parameters.AddWithValue("name", nameA_value.Text);
                    command.Parameters.AddWithValue("id_team", Convert.ToInt32(idTeam_value.Text));
                    command.Parameters.AddWithValue("particip_in_projects", Convert.ToInt32(partProj_value.Text));
                    command.Parameters.AddWithValue("experience", Convert.ToInt32(exp_value.Text));
                    command.ExecuteNonQuery();
                    MessageBox.Show("Изменение данных произведено");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                surname_value.Clear();
                nameA_value.Clear();
                idTeam_value.Clear();
                partProj_value.Clear();
                exp_value.Clear();
            }

        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (check == false)//добавление
            {
                string commandString = "select C_project(@name_order, @date_end, @id_firm, @id_team, @id_customer)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("name_order", nameO_value.Text);
                    command.Parameters.AddWithValue("date_end", Convert.ToDateTime(Date_end_value.Text));
                    command.Parameters.AddWithValue("id_firm", Convert.ToInt32(id_firm_value.Text));
                    command.Parameters.AddWithValue("id_team", Convert.ToInt32(id_team_value.Text));
                    command.Parameters.AddWithValue("id_customer", Convert.ToInt32(id_cust_value.Text));
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                nameO_value.Clear();              
                id_firm_value.Clear();
                id_team_value.Clear();
                id_cust_value.Clear();
            }
            else //изменения
            {
                string commandString = "select e_project(@id_project, @name_order, @date_end, @id_firm, @id_team, @id_customer)";

                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    command.Parameters.AddWithValue("id_project", Convert.ToInt32(id_project_box.Text));
                    command.Parameters.AddWithValue("name_order", nameO_value.Text);
                    command.Parameters.AddWithValue("date_end", Convert.ToDateTime(Date_end_value.Text));
                    command.Parameters.AddWithValue("id_firm", Convert.ToInt32(id_firm_value.Text));
                    command.Parameters.AddWithValue("id_team", Convert.ToInt32(id_team_value.Text));
                    command.Parameters.AddWithValue("id_customer", Convert.ToInt32(id_cust_value.Text));
                    command.ExecuteNonQuery();
                    MessageBox.Show("Изменение данных произведено");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                nameO_value.Clear();                
                id_firm_value.Clear();
                id_team_value.Clear();
                id_cust_value.Clear();
            }

        }

        
    }
}
