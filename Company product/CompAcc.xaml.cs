﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Company_product.Forms
{

    public partial class CompAcc : Window
    {
        
        private static NpgsqlConnection connection;

        bool flagF; //если false - то добавление, а если true то изменения

        List<string> tableNames = new List<string>() {"firm","customer","associate","team","project"};

        public CompAcc(NpgsqlConnection conn)
        {
            InitializeComponent();
            connection = conn;
        }

        private void List(string tablename)
        {
            string commandString = "select * from L_" + tablename + "();";
            DataTable table = new DataTable(tablename);


            try
            {
                NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
                adapter.Fill(table);
                ((DataGrid)FindName(tablename + "_table")).ItemsSource = table.DefaultView;
            }
            catch(Exception ex)
            {              
                   // MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error); что бы не доставали экзепшены               
            }

        }




        private void Del(string tablename)
        {
            DataRowView drv;
            string commandString;
            try
            {
                if (((DataGrid)FindName(tablename + "_table")).SelectedItems.Count > 0)
                {
                    for (int j = 0; j < ((DataGrid)FindName(tablename + "_table")).SelectedItems.Count; j++)
                    {
                        drv = (DataRowView)((DataGrid)FindName(tablename + "_table")).SelectedItems[j];
                        commandString = "select D_" + tablename + "(" + drv[0].ToString() + ");";

                        try
                        {
                            NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                            command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                    }
                }
                else MessageBox.Show("Вы не выбрали строки", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Для того что бы удалить эти данные, удалите фирму к которой относятся эти данные", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }            
        }
        

        private void Refresh_On_Closed(object sender, EventArgs e)

        {
            Refresh();
        }

        private void Refresh()
        {
            for (int i = 0; i < tableNames.Count; i++)
            {
                List(tableNames[i]);
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)//Update
        {
            flagF = true;
            int i = 0;
            AddWin aw = new AddWin(i, connection, flagF);
            aw.ShowDialog();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e) //List
        {
            Refresh();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Del(((TabItem)tabC.SelectedItem).Header.ToString());
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)//Add
        {
            flagF = false;
            int i = 0;
            AddWin aw = new AddWin(i, connection, flagF);
            aw.ShowDialog();
        }       
        private void MenuItem_Click(object sender, RoutedEventArgs e)//выход из аккаунта
        {
            LoginWin log = new LoginWin();
            this.Close();
            log.Show();
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)//выход из программы
        {
            this.Close();
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            string cs1 = "alter sequence customer_id_customer_seq restart;";
            string cs2 = "alter sequence firm_id_firm_seq restart;";
            string cs3 = "alter sequence team_id_team_seq restart;";
            string cs4 = "alter sequence associate_id_associate_seq restart;";
            string cs5 = "alter sequence project_id_project_seq restart;";

            try
            {
                NpgsqlCommand command1 = new NpgsqlCommand(cs1, connection);
                command1.ExecuteNonQuery();
                NpgsqlCommand command2 = new NpgsqlCommand(cs2, connection);
                command2.ExecuteNonQuery();
                NpgsqlCommand command3 = new NpgsqlCommand(cs3, connection);
                command3.ExecuteNonQuery();
                NpgsqlCommand command4 = new NpgsqlCommand(cs4, connection);
                command4.ExecuteNonQuery();
                NpgsqlCommand command5 = new NpgsqlCommand(cs5, connection);
                command5.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error); что бы не доставали экзепшены               
            }
        }
    }
}


