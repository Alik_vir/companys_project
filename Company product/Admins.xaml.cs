﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Company_product.Forms;
using Npgsql;
using System.Data;
using Microsoft.Win32;
using System.IO;

namespace Company_product
{
    /// <summary>
    /// Логика взаимодействия для Admins.xaml
    /// </summary>
    public partial class Admins : Window
    {
        private static NpgsqlConnection connection;

        
      


        public Admins(NpgsqlConnection conn)
        {
            InitializeComponent();
            connection = conn;

        }
        
        private void Button_Click(object sender, RoutedEventArgs e)//создание юзеров
         {

            string commandString = "select c_users ( '" + name_U.Text + "', '" + pass_U.Password + "', '" + combGR.Text + "');";//создание юзера(который будет наследовать свои параметры у роли
            try
             {               
                NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                 command.ExecuteNonQuery();
                MessageBox.Show("Пользователь создан", "Complite", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
             catch (Exception ex)
             {
                 MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
             }
            name_U.Clear();
            pass_U.Clear();

        }

        

        private void Button_Click_1(object sender, RoutedEventArgs e)//удаление юзеров
        {
            string commandString = "select D_users('" + name_U.Text + "');";
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                command.ExecuteNonQuery();
                MessageBox.Show("Пользователь удален", "Complite", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            name_U.Clear();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e) //добовление в группу ролей
        {
            
            string commandString = "select C_group(@user_inp, @role_user);";
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                command.Parameters.AddWithValue("user_inp", Convert.ToString(name_UU.Text));
                command.Parameters.AddWithValue("role_user", Convert.ToString(combGR2.Text));
                command.ExecuteNonQuery();
                MessageBox.Show("Пользователь дабавлен в группу" + combGR2.Text, "Complite", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            name_UU.Clear();
            

        }

        private void Button_Click_3(object sender, RoutedEventArgs e) //удаление из группы ролей
        {
            string commandString = "select D_group(@user_inp, @role_user);";
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                command.Parameters.AddWithValue("user_inp",Convert.ToString(name_UU.Text));
                command.Parameters.AddWithValue("role_user", Convert.ToString(combGR2.Text));
                command.ExecuteNonQuery();
                MessageBox.Show("Пользователь удален из группы", "Complite", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            name_UU.Clear();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {





            LoginWin log = new LoginWin();
            this.Close();
            log.Show();
        }

        private void MenuItem_XML(object sender, RoutedEventArgs e)
        {
            {
                try
                {
                    SaveFileDialog saveFile = new SaveFileDialog();
                    saveFile.Filter = "Text files(*.xml)|*.xml";
                    saveFile.ShowDialog();

                    if (saveFile.FileName != "" && saveFile.FileName != null)
                    {
                        NpgsqlCommand cmd = new NpgsqlCommand("select database_to_xml(true,false,'')", connection);
                        File.WriteAllText(saveFile.FileName, cmd.ExecuteScalar().ToString());
                        
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }


        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e) //audit
        {
            {
                DataTable table = new DataTable();
                string commandString = "select * from L_audit();";
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(commandString, connection);
                    NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);

                    adapter.Fill(table);
                    audit_table.ItemsSource = table.DefaultView;



                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error); что бы не доставали экзепшены               
                }
            }
            {
                DataTable table2 = new DataTable();
                string commandString2 = "select * from compressed_audit;";
                NpgsqlCommand command2 = new NpgsqlCommand(commandString2, connection);
                NpgsqlDataAdapter adapter2 = new NpgsqlDataAdapter(command2);

                adapter2.Fill(table2);
                compress_audit_table.ItemsSource = table2.DefaultView;
            }
        }
    }

}