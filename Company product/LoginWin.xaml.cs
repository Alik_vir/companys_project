﻿using Npgsql;
using System;
using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Controls;


namespace Company_product.Forms
{
    public partial class LoginWin : Window
    {
        private static NpgsqlConnection connection;
        




        private bool ConnInit()
        {
            
            try
            {
                string userid = loginBox.Text;
                string password = passBox.Password;

                string connectionString = "Server=localhost; Port=5432; User Id =" + userid + "; Password=" + password + ";Database=glu;";

                connection = new NpgsqlConnection(connectionString);

                connection.Open();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public LoginWin()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (ConnInit())
            {
                CompAcc comp = new CompAcc(connection);      
                this.Close();
                comp.Show();                
            }
            else
                MessageBox.Show("Неправильный логин или пароль", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);

        }

        private void loginBox_GotFocus(object sender, RoutedEventArgs e)
        {
            loginBox.Focus();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           // NpgsqlCommand cmd = new NpgsqlCommand("select pg_has_role('admin','member')", connection);



            if (ConnInit())
            { 
                Admins adm = new Admins(connection);
                this.Close();
                adm.Show();
            
            }
            else
                MessageBox.Show("Неправильный логин или пароль", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        
    }
}
/*
NpgsqlConnection conn = new NpgsqlConnection(Data.connectString);
conn.Open(); 
NpgsqlDataReader dataReader;
Menu();
NpgsqlCommand cmd = new NpgsqlCommand("select pg_has_role('admin','member')", conn);
dataReader = cmd.ExecuteReader(); 
dataReader.Read(); 
if (dataReader[0].ToString() == "True") 
{*/