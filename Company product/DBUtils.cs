using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
 
namespace Tutorial.SqlConn
{
    class DBUtils
    {
        public static MySqlConnection GetDBConnection( )
        {
            string host = "127.0.0.1";
            int port = 5432;
            string database = "postgres";
            string username = "admin";
            string password = "1234";
 
            return DBPostgreSQLUtils.GetDBConnection(host, port, database, username, password);
        }
        
    }
}