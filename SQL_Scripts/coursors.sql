  DROP TABLE  compressed_audit;

CREATE TABLE compressed_audit
(
  log_id serial PRIMARY KEY, --id лога
  userid varchar NOT NULL, --user который изменил
  operation varchar NOT NULL, --операция, которую произвел user
  op_time varchar NOT NULL,	--время и дата когда user это сделал
  tab_name varchar NOT NULL, --в какой таблице user это сделал
  izm_tab varchar --что изменил user
);

create or replace function compress_audit() returns integer
AS $$
    declare cur cursor for select userid, operation, op_time, tab_name, izm_tab from audit order by log_id;
    usID text;
    op text;
    opTi text;
    tabName text;
    izmTab text;
    tableData record;
    i int;
begin
  open cur;
  op:= '';
  i:= 0;
    LOOP
        fetch cur into tableData;
        if not found and exists (select count(*) from audit having count(*) <> 0) then
    			insert into compressed_audit(userid, operation, op_time, tab_name, izm_tab) values (usId, op, opTi, tabName, izmTab);
    			exit;
    		elsif not found and exists (select count(*) from audit having count(*) = 0) then
        exit;
  		end if;
  		if op = '' then
  			usID := tableData.userid;
  			op := tableData.operation;
  			opTi := tableData.op_time;
  			tabName := tableData.tab_name;
        izmTab := tableData.izm_tab;
      end if;
  		if tabName = tableData.tab_name then
  			if exists (select count(*) from compressed_audit having count(*) <> 0) or i <> 0  then
          izmTab := izmTab || ' || ' || tableData.izm_tab;
          op := op || ',  ' || tableData.operation;

  			end if;
  		else
  			insert into compressed_audit (userid, operation, op_time, tab_name, izm_tab) values (usId, op, opTi, tabName, izmTab);
        usID := tableData.userid;
  			op := tableData.operation;
  			opTi := tableData.op_time;
  			tabName := tableData.tab_name;
        izmTab := tableData.izm_tab;
  		end if;
  		i := i + 1;
  	end loop;
  	close cur;
  	return 0;
  end;
  $$
  language plpgsql;


select compress_audit();
