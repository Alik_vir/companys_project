CREATE TABLE customer (
    id_customer int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    name_company varchar(64) CONSTRAINT error_name_comp_C  CHECK (name_company <> ''),
    city_customer varchar(64) CONSTRAINT error_city_C  CHECK (city_customer <> ''),
    profit_of_year numeric CONSTRAINT error_prof_year_C CHECK (profit_of_year > 0)
);


CREATE TABLE firm (
    id_firm  int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    name_firm varchar(64) CONSTRAINT error_name_firm_F CHECK (name_firm <> ''),
    date_create date CONSTRAINT error_date_comp_F  CHECK (now() > date_create)
);


CREATE TABLE team (
    id_team int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    id_firm int CONSTRAINT error_if_firm_TA  CHECK (id_firm > 0) REFERENCES firm ON DELETE CASCADE ON UPDATE CASCADE,
    quantity_human smallint CONSTRAINT error_human_TA  CHECK (quantity_human > 0),
    name_team varchar(64) CONSTRAINT error_name_team_TA  CHECK (name_team <> '')
);


CREATE TABLE associate (
    id_associate int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    surname varchar(64) CONSTRAINT error_surname_A  CHECK(surname > ''),
    name_ass varchar(64) CONSTRAINT error_name  CHECK (name_ass > ''),
    id_team int CONSTRAINT error_id_team_A  CHECK (id_team > 0) REFERENCES team ON DELETE CASCADE ON UPDATE CASCADE,
    particip_in_projects int CONSTRAINT error_part_proj_A  CHECK (particip_in_projects <> 0),
    experience smallint CONSTRAINT error_exp_A  CHECK (experience > 0)
);


CREATE TABLE project (
    id_project int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    name_order varchar(64) CONSTRAINT error_name_order_P  CHECK (name_order > ''),
    date_end date CONSTRAINT error_date_P  CHECK (now() < date_end),
    id_firm int CONSTRAINT error_id_firm_P  CHECK (id_firm > 0) REFERENCES firm ON DELETE CASCADE ON UPDATE CASCADE,
    id_team int CONSTRAINT error_id_team_P  CHECK (id_team > 0) REFERENCES team ON DELETE CASCADE ON UPDATE CASCADE,
    id_customer int CONSTRAINT error_id_sust_P  CHECK (id_customer > 0) REFERENCES customer ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE audit
(
  log_id serial PRIMARY KEY, --id лога
  userid varchar NOT NULL, --user который изменил
  operation varchar NOT NULL, --операция, которую произвел user
  op_time timestamp NOT NULL default now(),	--время и дата когда user это сделал
  tab_name varchar NOT NULL, --в какой таблице user это сделал
  primary_key bigint, --id строки из измененной таблицы
  izm_tab varchar --что изменил user

);

CREATE TABLE compressed_audit
(
  log_id serial PRIMARY KEY, --id лога
  userid varchar NOT NULL, --user который изменил
  operation varchar NOT NULL, --операция, которую произвел user
  op_time varchar NOT NULL,	--время и дата когда user это сделал
  tab_name varchar NOT NULL, --в какой таблице user это сделал
  izm_tab varchar --что изменил user
);
