
--drop function ins() cascade;

CREATE OR REPLACE FUNCTION ins() RETURNS TRIGGER AS $$
declare
      primary_key_ bigint;
      izm varchar;
        BEGIN
        if(TG_OP = 'INSERT' ) THEN

                --firm
                if(TG_TABLE_NAME = 'firm') THEN
                        primary_key_ = NEW.id_firm;
                        izm = 'name: ' ||  NEW.name_firm || ', data: ' || NEW.date_create;
                --customer
                elseif(TG_TABLE_NAME = 'customer') THEN
                        primary_key_ = NEW.id_customer;
                        izm = 'name: ' ||  NEW.name_company || ', city: ' || NEW.city_customer || ', profit: ' || NEW.profit_of_year;
                --team
                elseif(TG_TABLE_NAME = 'team') THEN
                        primary_key_ = NEW.id_team;
                        izm = 'firm: ' ||  NEW.id_firm || ', quantity: ' || NEW.quantity_human || ', name_T: ' || NEW.name_team;
                --associate
                elseif(TG_TABLE_NAME = 'associate') THEN
                        primary_key_ = NEW.id_associate;
                        izm = 'surname: ' ||  NEW.surname || ', name: ' || NEW.name_ass || ', team: ' || NEW.id_team || ', particip: ' || NEW.particip_in_projects || ', exp: ' || NEW.experience;
                --project
                elseif(TG_TABLE_NAME = 'project') THEN
                        primary_key_ = NEW.id_project;
                        izm = 'name: ' ||  NEW.name_order || ', date: ' || NEW.date_end || ', firm: ' || NEW.id_team || ', team: ' || NEW.id_team || ', customer: ' || NEW.id_customer;
                end if;

        insert into audit (userid, operation, op_time, tab_name, primary_key, izm_tab) values (session_user, TG_OP, now(), TG_TABLE_NAME, primary_key_, izm);

end if;
  return null;
        END;

$$ LANGUAGE plpgsql;

--TRIGGERS
CREATE TRIGGER audit_ins
AFTER INSERT OR UPDATE OR DELETE ON firm
    FOR EACH ROW EXECUTE PROCEDURE ins();

CREATE TRIGGER audit_ins
AFTER INSERT OR UPDATE OR DELETE ON customer
    FOR EACH ROW EXECUTE PROCEDURE ins();

CREATE TRIGGER audit_ins
AFTER INSERT OR UPDATE OR DELETE ON associate
    FOR EACH ROW EXECUTE PROCEDURE ins();

CREATE TRIGGER audit_ins
AFTER INSERT OR UPDATE OR DELETE ON project
    FOR EACH ROW EXECUTE PROCEDURE ins();

CREATE TRIGGER audit_ins
AFTER INSERT OR UPDATE OR DELETE ON team
    FOR EACH ROW EXECUTE PROCEDURE ins();

        /* --примеры
        select c_firm('asdad','2000-10-10');
        select C_customer('asd','asdas', 123);
        select c_team(1, 1, 'idiots');
        select C_associate('asds','dasd', 1, 1, 1);
        select C_project('asd', '2019-10-10', 1, 1, 1); */

--DELETE
CREATE OR REPLACE FUNCTION del() RETURNS TRIGGER AS $$
declare
      primary_key_ bigint;
      izm varchar;
        BEGIN
        if(TG_OP = 'DELETE' ) THEN

                --firm
                if(TG_TABLE_NAME = 'firm') THEN
                        primary_key_ = OLD.id_firm;
                        izm = 'name: ' ||  OLD.name_firm || ', data: ' || OLD.date_create;
                --customer
                elseif(TG_TABLE_NAME = 'customer') THEN
                        primary_key_ = OLD.id_customer;
                        izm = 'name: ' ||  OLD.name_company || ', city: ' || OLD.city_customer || ', profit: ' || OLD.profit_of_year;
                --team
                elseif(TG_TABLE_NAME = 'team') THEN
                        primary_key_ = OLD.id_team;
                        izm = 'firm: ' ||  OLD.id_firm || ', quantity: ' || OLD.quantity_human || ', name_T: ' || OLD.name_team;
                --associate
                elseif(TG_TABLE_NAME = 'associate') THEN
                        primary_key_ = OLD.id_associate;
                        izm = 'surname: ' ||  OLD.surname || ', name: ' || OLD.name_ass || ', team: ' || OLD.id_team || ', particip: ' || OLD.particip_in_projects || ', exp: ' || OLD.experience;
                --project
                elseif(TG_TABLE_NAME = 'project') THEN
                        primary_key_ = OLD.id_project;
                        izm = 'name: ' ||  OLD.name_order || ', date: ' || OLD.date_end || ', firm: ' || OLD.id_team || ', team: ' || OLD.id_team || ', customer: ' || OLD.id_customer;
                end if;

        insert into audit (userid, operation, op_time, tab_name, primary_key, izm_tab) values (session_user, TG_OP, now(), TG_TABLE_NAME, primary_key_, izm);

end if;
  return null;
        END;

$$ LANGUAGE plpgsql;



--TRIGGERS
CREATE TRIGGER audit_del
AFTER INSERT OR UPDATE OR DELETE ON firm
    FOR EACH ROW EXECUTE PROCEDURE del();

CREATE TRIGGER audit_del
AFTER INSERT OR UPDATE OR DELETE ON customer
    FOR EACH ROW EXECUTE PROCEDURE del();

CREATE TRIGGER audit_del
AFTER INSERT OR UPDATE OR DELETE ON associate
    FOR EACH ROW EXECUTE PROCEDURE del();

CREATE TRIGGER audit_del
AFTER INSERT OR UPDATE OR DELETE ON project
    FOR EACH ROW EXECUTE PROCEDURE del();

CREATE TRIGGER audit_del
AFTER INSERT OR UPDATE OR DELETE ON team
    FOR EACH ROW EXECUTE PROCEDURE del();

/* --пример
    select d_firm(1);
    select d_customer(1);
    select d_team(1);
    select d_associate(1);
    select d_project(1); */


--UPDATE
CREATE OR REPLACE FUNCTION upd() RETURNS TRIGGER AS $$
declare
      primary_key_ bigint;
      izm varchar;
        BEGIN
        if(TG_OP = 'UPDATE' ) THEN

                --firm
                if(TG_TABLE_NAME = 'firm') THEN
                        primary_key_ = NEW.id_firm;
                        izm = 'old_name: ' ||  OLD.name_firm || ', old_data: ' || OLD.date_create  || ' | new_name: ' || NEW.name_firm || ', new_data: ' || NEW.date_create;
                --customer
                elseif(TG_TABLE_NAME = 'customer') THEN
                        primary_key_ = NEW.id_customer;
                        izm = 'old_name: ' ||  OLD.name_company  || ', old_city: ' || OLD.city_customer  || ', old_profit: ' || OLD.profit_of_year || ' | new_name: ' ||  NEW.name_company ||  ', new_city: ' || NEW.city_customer   ', new_profit: ' || NEW.profit_of_year;
                --team
                elseif(TG_TABLE_NAME = 'team') THEN
                        primary_key_ = NEW.id_team;
                        izm = 'old_firm: ' ||  OLD.id_firm || ', old_quantity: ' || OLD.quantity_human || ', old_name_T: ' || OLD.name_team || 'new_firm: ' ||  NEW.id_firm || ', new_quantity: ' || NEW.quantity_human || ', new_name_T: ' || NEW.name_team;
                --associate
                elseif(TG_TABLE_NAME = 'associate') THEN
                        primary_key_ = NEW.id_associate;
                        izm = 'old_surname: ' ||  OLD.surname || ', old_name: ' || OLD.name_ass || ', old_team: ' || OLD.id_team || ', old_particip: ' || OLD.particip_in_projects || ', old_exp: ' || OLD.experience || 'new_surname: ' ||  NEW.surname || ', new_name: ' || NEW.name_ass || ', new_team: ' || NEW.id_team || ', new_particip: ' || NEW.particip_in_projects || ', new_exp: ' || NEW.experience;
                --project
                elseif(TG_TABLE_NAME = 'project') THEN
                        primary_key_ = NEW.id_project;
                        izm = 'old_name: ' ||  OLD.name_order || ', old_date: ' || OLD.date_end || ', old_firm: ' || OLD.id_team || ', old_team: ' || OLD.id_team || ', old_customer: ' || OLD.id_customer || 'new_name: ' ||  NEW.name_order || ', new_date: ' || NEW.date_end || ', new_firm: ' || NEW.id_team || ', new_team: ' || NEW.id_team || ', new_customer: ' || NEW.id_customer;
                end if;

        insert into audit (userid, operation, op_time, tab_name, primary_key, izm_tab) values (session_user, TG_OP, now(), TG_TABLE_NAME, primary_key_, izm);

end if;
  return null;
        END;

$$ LANGUAGE plpgsql;



    --TRIGGERS
    CREATE TRIGGER audit_upd
    AFTER INSERT OR UPDATE OR DELETE ON firm
        FOR EACH ROW EXECUTE PROCEDURE upd();

    CREATE TRIGGER audit_upd
    AFTER INSERT OR UPDATE OR DELETE ON customer
        FOR EACH ROW EXECUTE PROCEDURE upd();

    CREATE TRIGGER audit_upd
    AFTER INSERT OR UPDATE OR DELETE ON associate
        FOR EACH ROW EXECUTE PROCEDURE upd();

    CREATE TRIGGER audit_upd
    AFTER INSERT OR UPDATE OR DELETE ON project
        FOR EACH ROW EXECUTE PROCEDURE upd();

    CREATE TRIGGER audit_upd
    AFTER INSERT OR UPDATE OR DELETE ON team
        FOR EACH ROW EXECUTE PROCEDURE upd();
/*
        --пример
        select E_firm(1,'ALEX','1998-10-22');
        select E_customer(1,'ALEX','ALEX', 100);
        select E_team(1 ,1, 1, 'ALEX');
        select E_associate(1, 'ALEX','ALEX', 1, 1, 1);
        select E_project(1, 'ALEX', '2029-01-01', 1, 1, 1); */
