--создание главных ролей и дача им прав
--grant select on firm to teaml;
--
--
--
--
--
/* SELECT grantee, privilege_type
FROM information_schema.role_table_grants
WHERE table_name='firm'; */

--Администратор аккаунтов true
	--users
create role Sys_admin login;
		grant execute on function public.C_users(text,text,text) to Sys_admin;

--Сотрудники true
create role associate login;


	grant execute on function public.L_project() to associate;
	grant execute on function public.L_team() to associate;



--Тим лидеры(сеньеры) true
create role teamL login;

	--view
	grant execute on function public.L_firm() to teaml;
	grant execute on function public.L_customer() to teaml;
	grant execute on function public.L_associate() to teaml;
	grant execute on function public.L_project() to teaml;
	grant execute on function public.L_team() to teaml;

	--control
	grant execute on function public.C_team(int,int,varchar) to teaml;
	grant execute on function public.D_team(int) to teaml;
	grant execute on function public.E_team(int,int,int,varchar) to teaml;


--Менеджер(по персоналу, компаний и сотрудникам) true
create role manager login;

	--control project
	grant execute on function public.L_project() to manager;
	grant execute on function public.C_project(varchar,timestamp,int,int,int) to manager;
	grant execute on function public.D_project(int) to manager;
	grant execute on function public.E_project(int,varchar,timestamp,int,int,int) to manager;

	--control firm
	grant execute on function public.L_firm() to manager;
	grant execute on function public.C_firm(varchar, timestamp) to manager;
	grant execute on function public.D_firm(int) to manager;
	grant execute on function public.E_firm(int, varchar, timestamp) to manager;

	--control associate
	grant execute on function public.L_associate() to manager;
	grant execute on function public.C_associate(varchar, varchar, int, int, int) to manager;
	grant execute on function public.D_associate(int) to manager;
	grant execute on function public.E_associate(int,varchar, varchar, int, int, int) to manager;

	--control customer
	grant execute on function public.L_customer() to manager;
	grant execute on function public.C_customer(varchar, varchar,numeric) to manager;
	grant execute on function public.D_customer(int) to manager;
	grant execute on function public.E_customer(int,varchar, varchar,numeric) to manager;

--Компании true
create role firms login;

	grant execute on function public.L_project() to firms;
	grant execute on function public.L_team() to firms;
	grant execute on function public.L_associate() to firms;
	grant execute on function public.L_customer() to firms;
