/*===============================================================================================================================================*/
/*FUNCTION*/
/*===============================================================================================================================================*/
---------------------------------------------------------------------------------------------------------------------------------------------------
----#################-----#--------------##-----##--------------#-----##################-----------------------------------------------------------
----##--------------------#-------------#-#-----#-#-------------#-----##---------------------------------------------------------------------------
----##--------------------#------------#--#-----#--#------------#-----##---------------------------------------------------------------------------
----##--------------------#-----------#---#-----#---#-----------#-----##---------------------------------------------------------------------------
----#################-----#----------#----#-----#----#----------#-----##---------------------------------------------------------------------------
----##--------------------#---------#-----#-----#-----#---------#-----##---------------------------------------------------------------------------
----##--------------------#--------#------#-----#------#--------#-----##---------------------------------------------------------------------------
----#################-----#-------#-------#-----#-------#-------#-----##---------------------------------------------------------------------------
----##--------------------#------#--------#-----#--------#------#-----##---------------------------------------------------------------------------
----##--------------------#-----#---------#-----#---------#-----#-----##---------------------------------------------------------------------------
----##--------------------#----#----------#-----#----------#----#-----##---------------------------------------------------------------------------
----##--------------------#---#-----------#-----#-----------#---#-----##---------------------------------------------------------------------------
----##--------------------#--#------------#-----#------------#--#-----##---------------------------------------------------------------------------
----##--------------------#-#-------------#-----#-------------#-#-----##---------------------------------------------------------------------------
----##--------------------##--------------#-----#--------------##-----##################-----------------------------------------------------------
/*===============================================================================================================================================*/
--create user фдуч with password '1234' in role admin
-- drop function c_users;

--users
--создание юзера
CREATE FUNCTION C_users(user_inp text, password_inp text, role_user text) RETURNS int --создание юзера с наследованием
AS $$
	begin
	execute 'create user ' || user_inp || ' with password ''' || password_inp || ''' in role '|| role_user || ';';
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление юзера
CREATE FUNCTION D_users(user_inp text) RETURNS int --удаление юзера
AS $$
	begin
	execute 'drop user ' || user_inp || ';';
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--grant

--удаление связи
 CREATE FUNCTION D_group(user_inp varchar, role_user varchar) RETURNS int --
AS $$
	begin
	execute 'alter group ' || role_user || ' drop user ' || user_inp || ' ;';
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


--добавление связи
CREATE FUNCTION C_group(user_inp varchar, role_user varchar) RETURNS int --
AS $$
 begin
 execute 'alter group ' || role_user || ' add user ' || user_inp || ';';
	 return 0;
 end;
$$ LANGUAGE plpgsql SECURITY DEFINER;




-- select c_users('alex', '1234');

--лист логов
CREATE  FUNCTION L_audit() returns SETOF audit
AS $$
		SELECT  log_id,
						userid,
						operation,
						op_time,
						tab_name,
						primary_key,
						izm_tab
		FROM audit;

$$ LANGUAGE sql SECURITY DEFINER;


/* CREATE  FUNCTION L_compressed_audit() returns SETOF compressed_audit
AS $$
		SELECT  log_id,
						userid,
						operation,
						op_time,
						tab_name,
						izm_tab
		FROM audit;

$$ LANGUAGE sql SECURITY DEFINER; */


--Лист фирмы
CREATE  FUNCTION L_firm() returns SETOF firm
AS $$
		SELECT  id_firm,
				name_firm,
				date_create
		FROM firm;

$$ LANGUAGE sql SECURITY DEFINER;

--лист заказчика
CREATE  FUNCTION L_customer() returns SETOF customer
AS $$
		SELECT  id_customer,
				name_company,
				city_customer,
				profit_of_year
		FROM customer;

$$ LANGUAGE sql SECURITY DEFINER;

--лист тимыf
CREATE  FUNCTION L_team() returns SETOF team
AS $$
		SELECT  id_team,
				id_firm,
				quantity_human,
				name_team
		FROM team;

$$ LANGUAGE sql SECURITY DEFINER;

--лист сотрудников
CREATE  FUNCTION L_associate() returns SETOF associate
AS $$
		SELECT  id_associate,
				surname,
				name_ass,
				id_team,
				particip_in_projects,
				experience
		FROM associate;

$$ LANGUAGE sql SECURITY DEFINER;

--лист проектов
CREATE  FUNCTION L_project() returns SETOF project
AS $$
		SELECT  id_project,
				name_order,
				date_end,
				id_firm,
				id_team,
				id_customer
		FROM project;

$$ LANGUAGE sql SECURITY DEFINER;



--добавление фирмы
CREATE FUNCTION C_firm(name_firm_inp varchar(64), date_create_inp timestamp) RETURNS int
AS $$
declare
	result int;
	begin

		INSERT INTO firm (name_firm, date_create) values (name_firm_inp, date_create_inp)

		returning id_firm into result;
		return result;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление фирмы

CREATE FUNCTION D_firm(id_firm_inp int) RETURNS int
AS $$
	begin

		DELETE FROM firm WHERE id_firm = id_firm_inp;

		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение фирмы
CREATE FUNCTION E_firm (id_firm_inp int, name_firm_inp varchar(64), date_create_inp timestamp) RETURNS int
AS $$
	begin

		UPDATE firm SET name_firm = name_firm_inp, date_create = date_create_inp WHERE id_firm = id_firm_inp;

		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;



/*===============================================================================================================================================*/
--CUSTOMER


--добавление
CREATE FUNCTION C_customer (name_comp_inp varchar(64), city_inp varchar(64), profit_of_year_inp numeric) RETURNS int
AS $$
	declare
		result int;
	begin
		INSERT INTO customer (name_company, city_customer, profit_of_year) values (name_comp_inp, city_inp, profit_of_year_inp)
		returning id_customer into result;
		return result;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление
CREATE FUNCTION D_customer (id_customer_inp int) RETURNS int
AS $$
	begin
		DELETE FROM customer WHERE id_customer = id_customer_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение
CREATE FUNCTION E_customer (id_customer_inp int, name_comp_inp varchar(64), city_inp varchar(64), profit_of_year_inp numeric) RETURNS int
AS $$
	begin
		UPDATE customer SET name_company = name_comp_inp, city_customer = city_inp, profit_of_year = profit_of_year_inp WHERE id_customer = id_customer_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


/*===============================================================================================================================================*/
--TEAM


--добавление
CREATE FUNCTION C_team (id_firm_inp int, quantity_human_inp int, name_team_inp varchar(64)) RETURNS int
AS $$
	declare
		result int;
	begin

		INSERT INTO  team (id_firm, quantity_human, name_team) values (id_firm_inp, quantity_human_inp, name_team_inp)

		returning id_team into result;
		return result;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление
CREATE FUNCTION D_team (id_team_inp int) RETURNS int
AS $$
	begin
		DELETE FROM team WHERE id_team = id_team_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение
CREATE FUNCTION E_team (id_team_inp int, id_firm_inp int, quantity_human_inp int, name_team_inp varchar(64)) RETURNS int
AS $$
	begin
		UPDATE team SET quantity_human = quantity_human_inp, name_team = name_team_inp WHERE id_team = id_team_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


/*===============================================================================================================================================*/
--ASSOCIATE

--добавление
CREATE FUNCTION C_associate (surname_inp varchar(64), name_inp varchar(64), id_team_inp int, particip_in_projects_inp int, experience_inp int) RETURNS int
AS $$
	declare
		result int;
	begin
		INSERT INTO associate (surname, name_ass, id_team, particip_in_projects, experience) values (surname_inp, name_inp, id_team_inp, particip_in_projects_inp, experience_inp)
		returning id_associate into result;
		return result;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление
CREATE FUNCTION D_associate (id_ass_inp int) RETURNS int
AS $$
	begin
		DELETE FROM associate WHERE id_associate = id_ass_inp;
		result 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение
CREATE FUNCTION E_associate (id_ass_inp int, surname_inp varchar(64), name_inp varchar(64), id_team_inp int, PiP_inp int, experience_inp int) RETURNS int
AS $$

	begin

		if exists (SELECT id_associate FROM associate WHERE id_associate = id_ass_inp) then

		UPDATE associate SET surname = surname_inp, name_ass = name_inp, id_team = id_team_inp, particip_in_projects = PiP_inp, experience = experience_inp WHERE id_associate = id_ass_inp;

		else raise exception 'such associate does not exist';
		end if;
return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


/*===============================================================================================================================================*/
--PROJECT

--создание
CREATE FUNCTION C_project (name_order_inp varchar(64), date_end_inp timestamp, id_firm_inp int, id_team_inp int, id_customer_inp int) RETURNS int
AS $$
	declare
		result int;
	begin

		INSERT INTO project (name_order, date_end, id_firm, id_team, id_customer) values (name_order_inp, date_end_inp, id_firm_inp, id_team_inp, id_customer_inp)

		returning id_firm into result;
		return result;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление
CREATE FUNCTION D_project (id_proj_inp int) RETURNS int
AS $$
	begin
		DELETE FROM project WHERE id_project = id_proj_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение
CREATE FUNCTION E_project (id_proj_inp int, name_order_inp varchar(64), date_end_inp timestamp, id_firm_inp int, id_team_inp int, id_customer_inp int) RETURNS int
AS $$
	begin

		UPDATE project SET name_order = name_order_inp, date_end = date_end_inp, id_firm = id_firm_inp, id_team = id_team_inp, id_customer = id_customer_inp WHERE id_project = id_proj_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;



--очистка функций
revoke all on function c_users from PUBLIC;
revoke all on function d_users from PUBLIC;
revoke all on function C_group from PUBLIC;
revoke all on function D_group from PUBLIC;

revoke all on function l_firm from PUBLIC;
revoke all on function c_firm from PUBLIC;
revoke all on function D_firm from PUBLIC;
revoke all on function E_firm from PUBLIC;

revoke all on function L_customer from PUBLIC;
revoke all on function C_customer from PUBLIC;
revoke all on function D_customer from PUBLIC;
revoke all on function E_customer from PUBLIC;

revoke all on function L_associate from PUBLIC;
revoke all on function C_associate from PUBLIC;
revoke all on function d_associate from PUBLIC;
revoke all on function E_associate from PUBLIC;

revoke all on function L_team from PUBLIC;
revoke all on function C_team from PUBLIC;
revoke all on function d_team from PUBLIC;
revoke all on function e_team from PUBLIC;

revoke all on function L_project from PUBLIC;
revoke all on function C_project from PUBLIC;
revoke all on function d_project from PUBLIC;
revoke all on function e_project from PUBLIC;



















_________________________________________________________________










--Search firm
CREATE FUNCTION select_firm (id_firm_inp int)
RETURNS TABLE (new_name_firm varchar(64), new_date_create timestamp)
AS $$
	begin
		return QUERY
		SELECT name_firm as new_name_firm, date_create as new_date_create FROM firm
		WHERE id_firm = id_firm_inp;

	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--Search customer
CREATE FUNCTION select_customer (id_cust_inp int)
RETURNS TABLE (new_name_comp varchar(64), new_city varchar(64), new_PiP numeric)
AS $$
	begin
		return QUERY
		SELECT name_company as new_name_comp, city as new_city, profit_of_year as new_PiP FROM customer
		WHERE id_customer = id_cust_inp;

	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--Search team
CREATE FUNCTION select_team (id_team_inp int)
RETURNS TABLE (new_firm int, new_quantity int, new_name_team varchar(64))
AS $$
	begin
		return QUERY
		SELECT id_firm as new_firm, quantity_human as new_quantity, name_team as new_name_team FROM team
		WHERE id_team = id_team_inp;

	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--Search associate
CREATE FUNCTION select_ass (id_ass_inp int)
RETURNS TABLE (new_sur varchar(64), new_name varchar(64), new_team int, new_PiP int, new_exp int)
AS $$
	begin
		return QUERY
		SELECT surname as new_sur, name as new_name, id_team as new_team, particip_in_projects as new_PiP, exception as new_PiP FROM associate
		WHERE id_associate = id_ass_inp;

	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--Search project
CREATE FUNCTION select_proj (id_proj_inp int)
RETURNS TABLE (new_name_ord varchar(64), new_date_end timestamp, new_firm int, new_team int, new_cust int)
AS $$
	begin
		return QUERY
		SELECT name_order as new_name_ord, date_end as new_date_end, id_firm as new_firm, id_team as new_team, id_customer as new_cust FROM team
		WHERE id_project = id_proj_inp;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;





/*
CREATE FUNCTION list_firm ()
RETURNS TABLE (new_id_firm int, new_name_firm varchar(64), new_date_create timestamp)
AS $$
	begin
		return QUERY
		SELECT id_firm as new_id_firm, name_firm as new_name_firm, date_create as new_date_create FROM firm;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER; */
